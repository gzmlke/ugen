const bc = require('babel-core');
const beautify = require('js-beautify').js_beautify;

const default_config = require('./default_config.js');
const processAst = require('./process_ast.js');
const consts = require('./consts.js');

const fileName = process.argv[2]; //todo
const custom_config = {}; //todo

const ast = bc.transformFileSync(fileName, {presets: ['react']}).ast;
const {tests, imports} = processAst(ast);

const config = Object.assign(default_config, custom_config);

console.log(beautify(config.getOutput({
	tests, imports, consts
})));
