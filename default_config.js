const config = {
	getSuiteName: function(context){
		return (context.test.superClass ? context.test.superClass + ' > ':'') + context.test.name;
	},
	getSuiteSetupBody(context){
		return 'this.sandbox=sinon.sandbox.create();';
	},
	getSuiteTeardownBody(context){
		return 'this.sandbox.restore();this.sandbox=null;';
	},
	getSuiteSetup: function(context){
		const body = this.getSuiteSetupBody(context);
		return `setup(function(){${body}});`;
	},
	getSuiteTeardown: function(context){
		const body = this.getSuiteTeardownBody(context);
		return `teardown(function(){${body}});`;
	},
	getSuiteStart: function(context){
		let code = '';
		const suiteName = this.getSuiteName(context);
		code += `suite('${suiteName}', function(){`;
		code += this.getSuiteSetup(context);
		code += this.getSuiteTeardown(context);
		return code;
	},
	getSuiteEnd: function(context){
		return '});';
	},
	getSuite: function(context){
		let code = '';
		code += this.getSuiteStart(context);
		context.tests.forEach((test)=>{
			Object.assign(context, {test});
			switch(test.type){
			case context.consts.TYPE_SUITE:			
			case context.consts.TYPE_CLASS:
				code += this.getSuite(Object.assign(Object.assign({}, context), {tests: test.methods}));
				break;
			case context.consts.TYPE_CLASS_METHOD:
				code += this.getClassMethodTest(context);
				break;
			case context.consts.TYPE_FUNCTION:
				code += this.getFunctionTest(context);
				break;
			}	
		});
		delete context.test;
		code += this.getSuiteEnd(context);
		return code;
	},

	getExternalFunctionCallReference(context){
		return `const ${context.ref.stubName} = this.sandbox.stub(${context.ref.path}, '${context.ref.name}');`;
	},

	getExternalReference(context){
		return '';
	},

	getFunctionTestName: function(context){
		return context.test.name;
	},
	getFunctionTestComments: function(context){
		let code = '';
		const importRefsNames = Object.keys(context.test.importRefs);
		code += '\n\n/**';
		code += `\n\t* Test function ${context.test.name}`;
		importRefsNames.forEach((id)=>{
			const ref = context.test.importRefs[id];
			if(ref.isCall){						
				code += `\n\t* @calls ${id} from ${ref.source}`;
			}else{
				code += `\n\t* @references ${id} from ${ref.source}`;
			}
		});
		code += '\n*/';
		return code;
	},
	getFunctionTestStart: function(context){
		let code = '';
		const testComments = this.getFunctionTestComments(context);
		const testName = this.getFunctionTestName(context);
		code += testComments;
		code += `test('${testName}', function(){`;
		return code;
	},
	getFunctionTestBody: function(context){
		let code = '';
		const importRefsNames = Object.keys(context.test.importRefs);
		importRefsNames.forEach((id)=>{
			const ref = context.test.importRefs[id];
			Object.assign(context, {ref});
			if(ref.isCall){						
				code += this.getExternalFunctionCallReference(context);
			}else{
				code += this.getExternalReference(context);
			}
			delete context.ref;
		});
		return code;
	},
	getFunctionTestEnd: function(context){
		return '});';
	},
	getFunctionTest: function(context){
		let code = '';
		code += this.getFunctionTestStart(context);
		code += this.getFunctionTestBody(context);
		code += this.getFunctionTestEnd(context);
		return code;
	},
	getClassMethodTest: function(context){
		return this.getFunctionTest(context);
	},

	getExternalImports(context){
		let code = '';
		context.imports.declarations.forEach((declaration)=>{
			const translatedSource = declaration.source;
			code += `import ${declaration.code} from '${translatedSource}';`;
		});		
		code += '\n\n';
		return code;
	},

	getOutput: function(context){
		let code = '';
		code = this.getExternalImports(context);
		context.tests.forEach((test)=>{
			Object.assign(context, {test});
			switch(test.type){
			case context.consts.TYPE_SUITE:			
			case context.consts.TYPE_CLASS:				
				code += this.getSuite(Object.assign(Object.assign({}, context), {tests: test.methods}));
				break;
			case context.consts.TYPE_CLASS_METHOD:
				code += this.getClassMethodTest(context);
				break;
			case context.consts.TYPE_FUNCTION:
				code += this.getFunctionTest(context);
				break;
			}	
		});
		delete context.test;
		return code;
	}
};

module.exports = config;