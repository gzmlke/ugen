const bc = require('babel-core');
const types = bc.types;

const consts = require('./consts.js');

function findImportReferences(path, imports){
	const importRefs = {};	
	path.traverse({	
		MemberExpression(path){
			const refObj = path.node.object.name;
			if(imports[refObj]){
				const ref = imports[refObj];
				let currentPath = path;
				let prevPath = path;
				const refChain = [refObj];
				while(types.isMemberExpression(currentPath.node)){
					refChain.push(currentPath.node.property.name);
					prevPath = currentPath;
					currentPath = currentPath.parentPath;
				}
				const fullName = refChain.join('.');
				const stubName = 'stub_'+refChain.join('_');
				const deepestName = refChain.pop();
				const flatRefChain = refChain.join('.');				
				const isCall = types.isCallExpression(currentPath.node) && currentPath.node.callee === prevPath.node;
				const key = `${fullName}${isCall?'()':''}`;
				importRefs[key]={
					path: flatRefChain,
					name: deepestName,
					source: ref.source,
					isCall,
					stubName
				};
			}
		}
	});
	return importRefs;
}

function processAst(ast){
	const imports = {
		keys: [],
		declarations: []
	};

	const globalFunctions = [];
	const tests = [];

	bc.traverse(ast, {
		ImportDeclaration(path){
			const source = path.node.source.value;
			const specifiers = path.node.specifiers;
			const specifiersCode = [];			
			const importSpecifiers = [];
			specifiers.forEach((specifier)=>{
				const localName = specifier.local.name;
				if(types.isImportDefaultSpecifier(specifier)){
					specifiersCode.push(localName);
				}else if(types.isImportSpecifier(specifier)){
					importSpecifiers.push(localName);
				}else if(types.isImportNamespaceSpecifier(specifier)){
					specifiersCode.push(`* as ${localName}`);
				}
				imports.keys.push(localName);
				imports[localName] = {
					source: source,
					importDeclaration: path.node,
					specifier: specifier
				};
			});
			
			if(importSpecifiers.length){
				specifiersCode.push(`{${importSpecifiers.join(', ')}}`);
			}
			imports.declarations.push({code: specifiersCode.join(', '), source});
		},
		ClassDeclaration(path){
			const name = path.node.id.name;
			const methods = [];
			path.traverse({
				ClassMethod(path){
					methods.push({
						type: consts.TYPE_CLASS_METHOD, 
						name: path.node.key.name,
						importRefs: findImportReferences(path, imports)
					});
				}
			});
			tests.push({
				type: consts.TYPE_CLASS, 
				superClass: path.node.superClass ? path.node.superClass.name : null,
				name,
				methods,
			});
		},
		FunctionDeclaration(path){
			globalFunctions.push({
				type: consts.TYPE_FUNCTION,
				name: path.node.id.name,
				importRefs: findImportReferences(path, imports)
			});
		}
	});

	if(globalFunctions.length){
		tests.push({
			type: consts.TYPE_SUITE,
			name: consts.SUITE_GLOBAL_FUNCTIONS,
			methods: globalFunctions
		});
	}

	return {
		tests,
		imports
	};
}

module.exports = processAst;